package org.gcube.data.publishing.gCatFeeder.service.model.fault;

public class DescriptorNotFound extends InternalError {

	public DescriptorNotFound() {
		// TODO Auto-generated constructor stub
	}

	public DescriptorNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DescriptorNotFound(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public DescriptorNotFound(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DescriptorNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
