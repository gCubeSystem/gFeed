package org.gcube.data.publishing.gCatFeeder.service.model.fault;

public class CollectorNotFound extends InternalError {

	public CollectorNotFound() {
		// TODO Auto-generated constructor stub
	}

	public CollectorNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CollectorNotFound(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CollectorNotFound(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CollectorNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
