package org.gcube.data.publishing.gCatFeeder.service.model.fault;

public class CataloguePluginNotFound extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2510068055460236480L;

	public CataloguePluginNotFound() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CataloguePluginNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CataloguePluginNotFound(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CataloguePluginNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CataloguePluginNotFound(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
