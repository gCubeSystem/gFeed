package org.gcube.data.publishing.gCatFeeder.service.model.fault;

public class PersistenceError extends InternalError {

	public PersistenceError() {
		// TODO Auto-generated constructor stub
	}

	public PersistenceError(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PersistenceError(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public PersistenceError(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PersistenceError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
