package org.gcube.data.publishing.gCatFeeder.service.model.fault;

public class InvalidRequest extends Exception {

	public InvalidRequest() {
		// TODO Auto-generated constructor stub
	}

	public InvalidRequest(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidRequest(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidRequest(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidRequest(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
