package org.gcube.data.publishing.gCatFeeder.service.model.fault;

public class InternalError extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5814890947362301499L;

	public InternalError() {
		// TODO Auto-generated constructor stub
	}

	public InternalError(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InternalError(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InternalError(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InternalError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
