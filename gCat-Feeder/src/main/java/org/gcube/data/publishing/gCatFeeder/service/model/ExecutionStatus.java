package org.gcube.data.publishing.gCatFeeder.service.model;

public enum ExecutionStatus {

	PENDING,
	RUNNING,
	STOPPED,
	FAILED,
	SUCCESS
	
}
