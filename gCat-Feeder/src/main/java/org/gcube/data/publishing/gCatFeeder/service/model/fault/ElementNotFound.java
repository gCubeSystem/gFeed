package org.gcube.data.publishing.gCatFeeder.service.model.fault;

public class ElementNotFound extends InvalidRequest {

	public ElementNotFound() {
		// TODO Auto-generated constructor stub
	}

	public ElementNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ElementNotFound(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ElementNotFound(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ElementNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
