This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.data-publishing.gFeed.gFeed-service

## [v1.0.6]
- Pom updates

## [v1.0.5]
- Pom updates

## [v1.0.4] - 2020-12-15
- Dependency management
- Naming Convention