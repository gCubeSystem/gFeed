package org.gcube.data.publishing.gCatFeeder.catalogues.model.faults;

public class WrongObjectFormatException extends Exception {

	public WrongObjectFormatException() {
		// TODO Auto-generated constructor stub
	}

	public WrongObjectFormatException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public WrongObjectFormatException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public WrongObjectFormatException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public WrongObjectFormatException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
