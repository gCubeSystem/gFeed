package org.gcube.data.publishing.gCatFeeder.catalogues.model.faults;

public class ControllerInstantiationFault extends Exception {

	public ControllerInstantiationFault() {
		// TODO Auto-generated constructor stub
	}

	public ControllerInstantiationFault(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ControllerInstantiationFault(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ControllerInstantiationFault(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ControllerInstantiationFault(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
