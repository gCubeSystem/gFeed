package org.gcube.data.publishing.gCatFeeder.catalogues.model.faults;

public class CatalogueInteractionException extends PublicationException {

	public CatalogueInteractionException() {
		// TODO Auto-generated constructor stub
	}

	public CatalogueInteractionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CatalogueInteractionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CatalogueInteractionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CatalogueInteractionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
