package org.gcube.data.publishing.gCatFeeder.catalogues.model.faults;

public class PublicationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2113773348645295768L;

	public PublicationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PublicationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public PublicationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PublicationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PublicationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
