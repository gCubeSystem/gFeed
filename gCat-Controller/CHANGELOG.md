This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.data-publishing.gFeed.gCat-controller

## [v1.0.8]
- Pom updates

## [v1.0.7]
- Pom updates

## [v1.0.6] - 2021-07-01
- gCat client coordinates 

## [v1.0.5] - 2020-12-15
- gCat Client upgrade


## [v1.0.4] - 2020-12-15
- Dependency management
- Naming Convention