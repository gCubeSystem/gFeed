package org.gcube.data.publishing.gCatFeeder.tests;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.GCubeSecret;
import org.gcube.common.authorization.utils.secret.JWTSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.data.publishing.gCatFeeder.model.EnvironmentConfiguration;
import org.gcube.data.publishing.gCatFeeder.utils.ISUtils;
import org.junit.BeforeClass;

public class InfrastructureTests {

	private static String testContext=null; 

	static {
		testContext=System.getProperty("testContext");		
		System.out.println("TEST CONTEXT = "+testContext);
	}

	protected static boolean isTestInfrastructureEnabled() {
		return testContext!=null;
	}

	@BeforeClass
	public static void setTestContext() {

		if(isTestInfrastructureEnabled()) {
			SecretManagerProvider.instance.set(new SecretManager());
			Properties props=new Properties();
			try{
				props.load(BaseCollectorTest.class.getResourceAsStream("/tokens.properties"));
			}catch(IOException e) {throw new RuntimeException(e);}
			if(!props.containsKey(testContext)) throw new RuntimeException("No token found for scope : "+testContext);

			String toSet = props.getProperty(testContext);

			Secret secret = null;
			if(toSet.length()>50)
				secret = new JWTSecret(toSet); // se nuovo token
			else
				secret = new GCubeSecret(toSet); // se vecchio token


			SecretManagerProvider.instance.get().addSecret(secret);
			try{
				SecretManagerProvider.instance.get().set();
			}catch(Exception e ){throw new RuntimeException("Unable to set secret for context "+testContext,e);}

		}
	}

	
	private static EnvironmentConfiguration env=new EnvironmentConfiguration() {
		
		@Override
		public Map<String, String> getCurrentConfiguration() {
			if(isTestInfrastructureEnabled()) {
				return ISUtils.loadConfiguration();
			}else return Collections.emptyMap();
		}
	};
	
	protected static EnvironmentConfiguration getEnvironmentConfiguration() {
		return env;
	}
}
