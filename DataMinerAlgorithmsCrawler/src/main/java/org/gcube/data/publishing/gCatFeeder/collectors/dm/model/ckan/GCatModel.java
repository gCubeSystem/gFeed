package org.gcube.data.publishing.gCatFeeder.collectors.dm.model.ckan;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;

import org.gcube.data.publishing.gCatFeeder.collectors.dm.DataMinerCollectorProperties;
import org.gcube.data.publishing.gCatFeeder.collectors.dm.URIResolver;
import org.gcube.data.publishing.gCatFeeder.collectors.dm.model.InternalAlgorithmDescriptor;
import org.gcube.data.publishing.gCatFeeder.collectors.dm.model.Parameter;
import org.gcube.data.publishing.gCatFeeder.collectors.dm.model.UserIdentity;
import org.gcube.data.publishing.gCatFeeder.model.CatalogueFormatData;
import org.gcube.data.publishing.gCatFeeder.model.InternalConversionException;
import org.gcube.data.publishing.gCatFeeder.utils.ContextUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Getter
@Setter
@Slf4j
public class GCatModel implements CatalogueFormatData {

	private static final DateTimeFormatter dateFormatter=DateTimeFormatter.ISO_DATE;


	private static ObjectMapper mapper=new ObjectMapper();

	private static String profileXML=null;

	String profileID=DataMinerCollectorProperties.getProperty(DataMinerCollectorProperties.CKAN_RESOURCE_TYPE);

	public static void setStaticProfile(String toSet) {
		profileXML=toSet;
	}



	public static String getItemUrl(String name) {
		try{
			URIResolver uriResolver = new URIResolver();
			log.debug("Evaluating item url for {}",name);
			String catalogueItemURL = uriResolver.getCatalogueItemURL(name);
			log.info("Item URL for {} is {}",name,catalogueItemURL);
			return catalogueItemURL;
		}catch(Exception e){
			log.warn("Unable to evaluate item URL for "+name,e);
			return null;
		}
	}

	public GCatModel(InternalAlgorithmDescriptor desc) {
		item=new CkanItem();
		item.setAuthor(desc.getAuthor().getFirstName()+" "+desc.getAuthor().getLastName());

		item.setTitle(desc.getName()+" in "+ContextUtils.getCurrentScopeName());
		item.setLicense_id("CC-BY-NC-SA-4.0");
		item.setMaintainer(desc.getMaintainer().getFirstName()+" "+desc.getMaintainer().getLastName());

		item.setName(item.getTitle().toLowerCase().toLowerCase().replaceAll(" ", "_"));
		for(String tag:desc.getTags()) {
			item.getTags().add(new CkanItem.Tag(fixTag(tag)));
		}
		item.getTags().add(new CkanItem.Tag(ContextUtils.getCurrentScopeName()));
		item.getTags().add(new CkanItem.Tag("WPS"));
		item.getTags().add(new CkanItem.Tag("Analytics"));
		item.getExtras().add(new CKanExtraField("system:type", profileID));

		item.setPrivateFlag(desc.getPrivateFlag());

		for(Parameter param: desc.getInputParameters())
			item.getExtras().add(new CKanExtraField("TechnicalDetails:input",
					String.format("%1$s [%2$s] %3$s : %4$s",
							param.getName(),param.getType(),
							((param.getValue()!=null&&!param.getValue().isEmpty())?"default : "+param.getValue():""),
							param.getDescription())));


		for(Parameter param: desc.getOutputParameters())
			item.getExtras().add(new CKanExtraField("TechnicalDetails:output",
					String.format("%1$s [%2$s] %3$s : %4$s",
							param.getName(),param.getType(),
							((param.getValue()!=null&&!param.getValue().isEmpty())?"default : "+param.getValue():""),
							param.getDescription())));


		item.getExtras().add(new CKanExtraField("Identity:Creator", desc.getAuthor().asStringValue()));


		item.getExtras().add(new CKanExtraField("Identity:CreationDate", dateFormatter.format(desc.getCreationDate())));

		item.getExtras().add(new CKanExtraField("AccessMode:UsageMode", "as-a-Service via Blue-Cloud Infrastructure"));
		item.getExtras().add(new CKanExtraField("AccessMode:Availability", "On-Line"));

		item.getExtras().add(new CKanExtraField("TechnicalDetails:Hosting Environment", "gCube SmartGear"));
		item.getExtras().add(new CKanExtraField("TechnicalDetails:Dependencies on Other SW", "gCube DataMiner"));

		item.getExtras().add(new CKanExtraField("Rights:Field/Scope of use", "Any use"));
		item.getExtras().add(new CKanExtraField("Rights:Basic rights", "Communication"));
		item.getExtras().add(new CKanExtraField("Rights:Basic rights", "Making available to the public"));
		item.getExtras().add(new CKanExtraField("Rights:Basic rights", "Distribution"));

		item.getExtras().add(new CKanExtraField("Attribution:Attribution requirements",
				String.format("Cite as: %1$s (%2$d): %3$s. %4$s. %5$s. %6$s. %7$s. Retrieved from the %8$s (%9$s) operated by D4Science.org www.d4science.org",
						desc.getAuthor().asStringValue(),
						LocalDateTime.now().getYear(),
						desc.getName(),
						desc.getVersion(),
						"Blue-Cloud",
						"DataMiner Process",
						getItemUrl(item.getName()),
						desc.getGatewayName(),
						desc.getGuiLink())));





		//Algorithm Description
		//		item.getExtras().add(new CKanExtraField(profileID+":Process Description", desc.getDescription()));

		item.setNotes(desc.getDescription());


		// Algorithm Users

		//item.getExtras().add(new CKanExtraField(profileID+":Process Author",desc.getAuthor().asStringValue()));
		//item.getExtras().add(new CKanExtraField(profileID+":Process Maintainer",desc.getAuthor().asStringValue()));

		if(desc.getGuiLink()!=null) {	
			try {
				URL url=new URL(desc.getGuiLink());
				resources.add(new CkanResource("Gateway Link",desc.getGuiLink(),url.getProtocol(),"Link to the GUI designed to operate with DataMiner"));
			}catch(Throwable t) {
				log.warn("Unable to generate resource from gui Link : "+desc.getGuiLink(),t);
			}
		}
		if(desc.getWpsLink()!=null)
			resources.add(new CkanResource("WPS Link", desc.getWpsLink(), "WPS","WPS Link to the "+DataMinerCollectorProperties.getProperty(DataMinerCollectorProperties.CKAN_RESOURCE_TYPE)));

	}

	@Setter
	private String profile=profileXML;

	private CkanItem item=null;
	private ArrayList<CkanResource> resources=new ArrayList<>();


	@Override
	public String toCatalogueFormat() throws InternalConversionException {
		try{
			ByteArrayOutputStream baos=new ByteArrayOutputStream();
			mapper.writeValue(baos, this);
			return baos.toString();
		}catch(Throwable t) {
			throw new InternalConversionException("Unable to convert",t);
		}
	}




	static final String fixTag(String toFix) {
		String fixedTag=toFix.replaceAll(":", " ").
				replaceAll("[\\(\\)\\\\/]", "-").replaceAll("[’']","_");
		if(fixedTag.length()>100)				
			fixedTag=fixedTag.substring(0,96)+"...";
		return fixedTag.trim();
	}

	static final String identityString(UserIdentity id) {
		StringBuilder builder=new StringBuilder(id.getLastName()+", ");
		builder.append(id.getFirstName()+", ");
		if(id.getEmail()!=null) builder.append(id.getEmail()+", ");
		if(id.getOrcid()!=null) builder.append(id.getOrcid()+", ");
		return builder.toString().substring(0,builder.lastIndexOf(","));
	}
}
