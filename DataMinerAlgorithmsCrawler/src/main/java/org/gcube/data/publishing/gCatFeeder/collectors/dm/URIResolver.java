package org.gcube.data.publishing.gCatFeeder.collectors.dm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.data.publishing.gCatFeeder.utils.ContextUtils;
import org.gcube.portlets.user.uriresolvermanager.UriResolverManager;
import org.gcube.portlets.user.uriresolvermanager.exception.IllegalArgumentException;
import org.gcube.portlets.user.uriresolvermanager.exception.UriResolverMapException;

public class URIResolver {


    private static final String CTLG_RESOLVER_NAME="CTLG";

   // private static final String CATALOGUE_CONTEXT = "gcube_scope";
    private static final String ENTITY_CONTEXT = "entity_context";
    private static final String ENTITY_NAME = "entity_name";

    private static final String DATASET = "dataset";

    protected ObjectMapper mapper;

    public URIResolver() {
        this.mapper = new ObjectMapper();
    }

    protected StringBuilder getStringBuilder(InputStream inputStream) throws IOException {
        StringBuilder result = new StringBuilder();
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while((line = reader.readLine()) != null) {
                result.append(line);
            }
        }
        return result;
    }


    /*
        {
        "gcube_scope" : "/gcube/devsec/devVRE",
        "entity_context" : "dataset",
        "entity_name" : "sarda-sarda"
        }
     */
    public String getCatalogueItemURL(String name) throws UriResolverMapException, IllegalArgumentException {
        UriResolverManager resolver = new UriResolverManager(CTLG_RESOLVER_NAME);
        Map<String, String> params = new HashMap<String, String>();
        params.put(ENTITY_NAME, name);
        params.put(ENTITY_CONTEXT,DATASET);
        params.put("gcube_scope", ContextUtils.getCurrentScope());
        String shortLink = resolver.getLink(params, false);
        return shortLink;
    }

    /*public String getCatalogueItemURL(String name) {
        try {
            String uriResolverURL = getConfigurationFromIS();

            ObjectNode requestContent = mapper.createObjectNode();
            requestContent.put(CATALOGUE_CONTEXT, ContextUtils.getCurrentScope());

            requestContent.put(ENTITY_TYPE, DATASET);
            requestContent.put(ENTITY_NAME, name);

            GXHTTPStringRequest gxhttpStringRequest = GXHTTPStringRequest.newRequest(uriResolverURL);
            gxhttpStringRequest.from(CTLG_RESOLVER_NAME);
            gxhttpStringRequest.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);
            gxhttpStringRequest.isExternalCall(true);
            String body = mapper.writeValueAsString(requestContent);
            HttpURLConnection httpURLConnection = gxhttpStringRequest.post(body);

            if(httpURLConnection.getResponseCode() != 200) {
                try{
                    IOUtils.copy(httpURLConnection.getInputStream(),System.out);
                }catch(Throwable t){
                    System.out.println("No message");
                }
                throw new InternalServerErrorException("Unable to get Item URL via URI Resolver. Code : "+
                        httpURLConnection.getResponseCode());
            }

            String url = getStringBuilder(httpURLConnection.getInputStream()).toString();

            return url;
        } catch(WebApplicationException e) {
            throw e;
        } catch(Exception e) {
            throw new WebApplicationException(e);
        }
    }*/


    /*
    static String getConfigurationFromIS() {
        try {

            StringBuilder toReturn=new StringBuilder();
            ServiceEndpoint serviceEndpoint = ISUtils.queryForServiceEndpointsByName("Service","HTTP-URI-Resolver").get(0);
            serviceEndpoint.profile().accessPoints().
                    forEach(a->{
                        if(a.name().equals(CTLG_RESOLVER_NAME))
                          toReturn.append(a.address());
                    });
            if(toReturn.length()>0) return toReturn.toString();
            else throw new Exception("Access point for "+CTLG_RESOLVER_NAME+" not found ");
        } catch(WebApplicationException e) {
            throw e;
        } catch(Exception e) {
            throw new InternalServerErrorException("Error while getting configuration on IS", e);
        }

    }

     */
}
