package org.gcube.data.publishing.gCatFeeder.collectors.dm;

public class Constants {

	public static final String GCAT_TYPE="GCAT";
	
	public static final String PLUGIN_ID="DATAMINER_ALGORITHMS_COLLECTOR"; 
	
	public static final String ENVIRONMENT_PROPERTIES_BASE=PLUGIN_ID+".";
	
	
	// ENVIRONMENT EXPECTED PARAMETERS
	public static final String GUI_BASE_URL=ENVIRONMENT_PROPERTIES_BASE+"GUI_BASE_URL";
	public static final String GATEWAY_NAME=ENVIRONMENT_PROPERTIES_BASE+"GATEWAY_NAME";

	public static final String DEFAULT_AUTHOR=ENVIRONMENT_PROPERTIES_BASE+"DEFAULT_AUTHOR";
	
	public static final String DEFAULT_MAINTAINER=ENVIRONMENT_PROPERTIES_BASE+"DEFAULT_MAINTAINER";
	
	public static final String PRIVATE=ENVIRONMENT_PROPERTIES_BASE+"PRIVATE";
	
}
