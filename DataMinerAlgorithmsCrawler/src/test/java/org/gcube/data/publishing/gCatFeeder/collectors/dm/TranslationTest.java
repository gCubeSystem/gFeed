package org.gcube.data.publishing.gCatFeeder.collectors.dm;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Set;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.data.publishing.gCatFeeder.collectors.dm.model.InternalAlgorithmDescriptor;
import org.gcube.data.publishing.gCatFeeder.collectors.dm.model.ckan.GCatModel;
import org.gcube.data.publishing.gCatFeeder.model.CatalogueFormatData;
import org.gcube.data.publishing.gCatFeeder.model.InternalConversionException;
import org.gcube.data.publishing.gCatFeeder.tests.BaseCollectorTest;
import org.gcube.data.publishing.gCatFeeder.tests.TokenSetter;
import org.gcube.data.publishing.gCatfeeder.collectors.CollectorPlugin;
import org.gcube.data.publishing.gCatfeeder.collectors.DataCollector;
import org.gcube.data.publishing.gCatfeeder.collectors.DataTransformer;
import org.gcube.data.publishing.gCatfeeder.collectors.model.faults.CatalogueNotSupportedException;
import org.gcube.data.publishing.gCatfeeder.collectors.model.faults.CollectorFault;
import org.junit.Assume;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.junit.Assert.*;

public class TranslationTest extends BaseCollectorTest{


	@Test
	public void testTranslation() throws CollectorFault, CatalogueNotSupportedException, JsonGenerationException, JsonMappingException, IOException, InternalConversionException {
		Assume.assumeTrue(isTestInfrastructureEnabled());
			System.out.println("Entering Infrastructure enabled tests..");
			ObjectMapper mapper = new ObjectMapper();

			CollectorPlugin plugin=new DataMinerPlugin();
			plugin.setEnvironmentConfiguration(getEnvironmentConfiguration());
			DataCollector collector=plugin.getCollector();
			Collection collected=collector.collect();
			System.out.println("Found "+collected.size()+" elements");
			for(Object obj:collected)
				System.out.println(mapper.writeValueAsString(obj)+"\n");

			for(String destinationcatalogue : (Set<String>)plugin.getSupportedCatalogueTypes()) {
				DataTransformer<? extends CatalogueFormatData, InternalAlgorithmDescriptor> transformer=
						plugin.getTransformerByCatalogueType(destinationcatalogue);
				System.out.println("Starting Transformation "+transformer.toString());

				for(Object data:transformer.transform(collected))
					System.out.println(((CatalogueFormatData)data).toCatalogueFormat());
			
		}
	}



	@Test
	public void testParseDescription(){
		String s="balablabalba";
		assertEquals("n/a",DMAlgorithmsInfoCollector.parseDescriptionForDate(s));
		assertEquals(null, DMAlgorithmsInfoCollector.parseDescriptionForUser(s));
		s="Basic statistic max min average {Published by Giancarlo Panichi (giancarlo.panichi) on 2018/07/20 10:24 GMT}";
		assertEquals("2018/07/20 10:24 GMT",DMAlgorithmsInfoCollector.parseDescriptionForDate(s));
		assertEquals("Giancarlo Panichi ",DMAlgorithmsInfoCollector.parseDescriptionForUser(s));



		System.out.println(DMAlgorithmsInfoCollector.versionDateParser.format(ZonedDateTime.now()));
		System.out.println(DMAlgorithmsInfoCollector.versionDateParser.parse(DMAlgorithmsInfoCollector.parseDescriptionForDate(s)));
	}

	@Test
	public void testEnvironment(){
		Assume.assumeTrue(isTestInfrastructureEnabled());
		//assertNotNull(DMAlgorithmsInfoCollector.getWPSBasePath());
		//assertNotNull(GCatModel.getItemUrl("fake"));
		System.out.println(getEnvironmentConfiguration().getCurrentConfiguration());
	}
}
