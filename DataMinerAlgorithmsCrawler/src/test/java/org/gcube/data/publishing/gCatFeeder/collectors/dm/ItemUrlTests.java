package org.gcube.data.publishing.gCatFeeder.collectors.dm;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.data.publishing.gCatFeeder.tests.BaseCollectorTest;
import org.gcube.data.publishing.gCatFeeder.tests.TokenSetter;
import org.gcube.portlets.user.uriresolvermanager.exception.IllegalArgumentException;
import org.gcube.portlets.user.uriresolvermanager.exception.UriResolverMapException;
import org.junit.Test;

public class ItemUrlTests extends BaseCollectorTest {

    @Test
    public void getItemURL() throws UriResolverMapException, IllegalArgumentException {
        TokenSetter.set("/pred4s/preprod/preVRE");
        String name = "fake";
        System.out.println(new URIResolver().getCatalogueItemURL(name));
    }
}
