This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.data-publishing.gFeed.DataMinerAlgorithmsCrawler

## [v1.0.7]
- Lombok version

## [v1.0.6]
- Pom updates

## [v1.0.5] - 2020-12-15
- Fixes [#22344](https://support.d4science.org/issues/22344#change-128440) : publish DM algorithms as Methods

## [v1.0.4] - 2020-12-15
- Dependency management
- Naming Convention