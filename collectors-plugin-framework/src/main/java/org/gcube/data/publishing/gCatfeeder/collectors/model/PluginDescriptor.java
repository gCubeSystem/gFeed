package org.gcube.data.publishing.gCatfeeder.collectors.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class PluginDescriptor {

	// Plugin info 
	
	@NonNull
	private String name;
	 
	
	// Run Interval
	
	
}
