package org.gcube.data.publishing.gCatfeeder.collectors.model.faults;

public class CollectorFault extends CrawlerException {

	public CollectorFault() {
		// TODO Auto-generated constructor stub
	}

	public CollectorFault(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CollectorFault(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CollectorFault(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CollectorFault(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
