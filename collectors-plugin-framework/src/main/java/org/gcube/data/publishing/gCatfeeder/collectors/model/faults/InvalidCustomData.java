package org.gcube.data.publishing.gCatfeeder.collectors.model.faults;

public class InvalidCustomData extends CrawlerException {

	public InvalidCustomData() {
		// TODO Auto-generated constructor stub
	}

	public InvalidCustomData(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidCustomData(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidCustomData(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidCustomData(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
