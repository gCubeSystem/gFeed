package org.gcube.data.publishing.gCatfeeder.collectors.model.faults;

public class CrawlerException extends Exception {

	public CrawlerException() {
		// TODO Auto-generated constructor stub
	}

	public CrawlerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CrawlerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CrawlerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CrawlerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
