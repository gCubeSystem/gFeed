package org.gcube.data.publishing.gCatfeeder.collectors.model.faults;

public class CatalogueNotSupportedException extends CrawlerException {

	public CatalogueNotSupportedException() {
		// TODO Auto-generated constructor stub
	}

	public CatalogueNotSupportedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CatalogueNotSupportedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CatalogueNotSupportedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CatalogueNotSupportedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
