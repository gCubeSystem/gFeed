package org.gcube.application.gfeed.oai;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.MalformedURLException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.gcube.data.publishing.gCatFeeder.model.InternalConversionException;
import org.gcube.data.publishing.gCatFeeder.tests.TokenSetter;
import org.gcube.data.publishing.gCatFeeder.utils.Files;
import org.gcube.data.publishing.gFeed.collectors.oai.model.DCRecordMetadata;
import org.gcube.data.publishing.gFeed.collectors.oai.model.MetadataHolder;
import org.gcube.data.publishing.gFeed.collectors.oai.model.OAIMetadata;
import org.gcube.data.publishing.gFeed.collectors.oai.model.OAIRecord;
import org.gcube.data.publishing.gFeed.collectors.oai.model.OAI_PMH;
import org.gcube.data.publishing.gFeed.collectors.oai.model.ckan.GCatModel;
import org.gcube.data.publishing.gFeed.collectors.oai.model.ckan.GCatTransformer;
import org.gcube.gcat.client.Item;

import com.fasterxml.jackson.databind.ObjectMapper;


public class CKANTest {

	
	public static void main (String args[]) throws JAXBException {
//		TokenSetter.set("/pred4s/preprod/preVRE");
//		
//		
//		File toRead=Files.getFileFromResources("resp_dc.xml");
//		
//		JAXBContext jaxbContext = JAXBContext.newInstance(OAIRecord.class,
//		    		MetadataHolder.class,
//		    		OAIMetadata.class,
//		    		DCRecordMetadata.class,
//		    		OAI_PMH.class);             
//		
//	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//	 
//	    OAI_PMH response = (OAI_PMH) jaxbUnmarshaller.unmarshal(toRead);
//	    
//	    for(GCatModel gcat: new GCatTransformer().transform(response.getResponseRecords().getRecords())) {
//	    	if(gcat.)
//	    }
//	    
	    
	}
	
	
	private static ObjectMapper mapper=new ObjectMapper();
	
	
	public static void updateItem(String name, String itemContent) throws MalformedURLException {
		new Item().update(name, itemContent);
	}
	
	public static void createItem(String itemContent) throws MalformedURLException {
		new Item().create(itemContent);
	}
	
	
	public static void getItem(String name) throws MalformedURLException {
		new Item().read(name);
	}
	
	public String toCatalogueFormat() throws InternalConversionException {
		try{
			ByteArrayOutputStream baos=new ByteArrayOutputStream();
			mapper.writeValue(baos, this);
			return baos.toString();
		}catch(Throwable t) {
			throw new InternalConversionException("Unable to convert",t);
		}
	}
	
	
	
}
