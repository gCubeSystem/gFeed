package org.gcube.application.gfeed.oai;

import java.io.IOException;
import java.util.Collection;
import java.util.Set;

import org.gcube.data.publishing.gCatFeeder.model.CatalogueFormatData;
import org.gcube.data.publishing.gCatFeeder.model.EnvironmentConfiguration;
import org.gcube.data.publishing.gCatFeeder.model.InternalConversionException;
import org.gcube.data.publishing.gCatFeeder.tests.BaseCollectorTest;
import org.gcube.data.publishing.gCatfeeder.collectors.CollectorPlugin;
import org.gcube.data.publishing.gCatfeeder.collectors.DataCollector;
import org.gcube.data.publishing.gCatfeeder.collectors.DataTransformer;
import org.gcube.data.publishing.gCatfeeder.collectors.model.faults.CatalogueNotSupportedException;
import org.gcube.data.publishing.gCatfeeder.collectors.model.faults.CollectorFault;
import org.gcube.data.publishing.gFeed.collectors.oai.OAIHarvester;
import org.gcube.data.publishing.gFeed.collectors.oai.model.OAIRecord;
import org.junit.Assume;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TranslationTest extends BaseCollectorTest{

	@Test
	public void testTranslation() throws CollectorFault, CatalogueNotSupportedException, JsonGenerationException, JsonMappingException, IOException, InternalConversionException {
		Assume.assumeTrue(isTestInfrastructureEnabled());

		System.out.println("Entering Infrastructure enabled tests..");
		ObjectMapper mapper = new ObjectMapper();

		CollectorPlugin plugin=new OAIHarvester();
		EnvironmentConfiguration env = getEnvironmentConfiguration();
		System.out.println("Setting ENV : "+env);
		plugin.setEnvironmentConfiguration(env);
		DataCollector collector=plugin.getCollector();
		Collection collected=collector.collect();
		System.out.println("Found "+collected.size()+" elements");
		for(Object obj:collected)
			System.out.println(mapper.writeValueAsString(obj)+"\n");

//		for(String destinationcatalogue : (Set<String>)plugin.getSupportedCatalogueTypes()) {
//			DataTransformer<? extends CatalogueFormatData, OAIRecord> transformer=plugin.getTransformerByCatalogueType(destinationcatalogue);
//			for(Object data:transformer.transform(collected))
//				System.out.println(((CatalogueFormatData)data).toCatalogueFormat());
//		}
	}
	
	
	
	
}
