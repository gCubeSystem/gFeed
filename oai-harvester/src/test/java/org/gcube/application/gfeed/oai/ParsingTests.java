package org.gcube.application.gfeed.oai;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.gcube.data.publishing.gCatFeeder.model.InternalConversionException;
import org.gcube.data.publishing.gCatFeeder.utils.Files;
import org.gcube.data.publishing.gCatFeeder.utils.IOUtils;
import org.gcube.data.publishing.gFeed.collectors.oai.model.DCRecordMetadata;
import org.gcube.data.publishing.gFeed.collectors.oai.model.MetadataHolder;
import org.gcube.data.publishing.gFeed.collectors.oai.model.OAIMetadata;
import org.gcube.data.publishing.gFeed.collectors.oai.model.OAIRecord;
import org.gcube.data.publishing.gFeed.collectors.oai.model.OAI_PMH;
import org.gcube.data.publishing.gFeed.collectors.oai.model.ckan.GCatModel;
import org.gcube.data.publishing.gFeed.collectors.oai.model.ckan.GCatTransformer;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ParsingTests {

	static JAXBContext jaxbContext;
	
	@BeforeClass
	public static void init() throws JAXBException {
		
	    jaxbContext = JAXBContext.newInstance(OAIRecord.class,
	    		MetadataHolder.class,
	    		OAIMetadata.class,
	    		DCRecordMetadata.class,
	    		OAI_PMH.class);              
	}
	
//	@Test
	public void parseDC() throws JAXBException, IOException {
		
		OAI_PMH response=read();
		    Assert.assertNotNull(response);
		    Assert.assertNotNull(response.getResponseRecords().getRecords());
		    
		    assertTrue(response.getResponseRecords().getRecords().size()>0);
		    
		    for(OAIRecord record : response.getResponseRecords().getRecords()) {
		    	log.debug("Record is "+record);
		    }
		    
//		    System.out.println(response);
		
		   
	}

//	@Test
	public void marshal() throws JAXBException, InternalConversionException, IOException {
		OAI_PMH resp=read();
		File outF=File.createTempFile("json", ".json");
		PrintWriter out = new PrintWriter(outF);
		 for(GCatModel gcat: new GCatTransformer().transform(resp.getResponseRecords().getRecords())) {
			 Assert.assertNotNull(gcat.getResources());
			 Assert.assertFalse(gcat.getResources().isEmpty());
			 out.println(gcat.toCatalogueFormat());
		 }
		out.flush();
		out.close();
		System.out.println("Output at "+outF.getAbsolutePath());
	}
	
	private OAI_PMH read() throws JAXBException, IOException {
		String toRead=IOUtils.readStream(
				getClass().getClassLoader().getResourceAsStream("resp_dc.xml"));
		
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	 
	    return (OAI_PMH) jaxbUnmarshaller.unmarshal(new StringReader(toRead));
	     
	}
	
}
