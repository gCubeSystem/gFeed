package org.gcube.data.publishing.gFeed.collectors.oai.model.ckan;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
public class CkanItem {

	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@ToString
	public static class CKanExtraField {

		private String key;
		private String value;
	}
	
	@Getter
	@Setter
	@NoArgsConstructor
	@RequiredArgsConstructor
	@ToString
	public static class Tag{
		@NonNull
		private String name;
	}
	

	
	private String name;
	private String title;	
	private String version;
	@JsonProperty("private")
	private Boolean privateFlag;
	private String license_id;
	private String author;
	private String maintainer;
	private String notes;
	private ArrayList<Tag> tags=new ArrayList<Tag>();

	private ArrayList<CKanExtraField> extras=new ArrayList<>();
}