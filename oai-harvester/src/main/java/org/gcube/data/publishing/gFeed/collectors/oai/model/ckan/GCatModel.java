package org.gcube.data.publishing.gFeed.collectors.oai.model.ckan;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import org.gcube.data.publishing.gCatFeeder.model.CatalogueFormatData;
import org.gcube.data.publishing.gCatFeeder.model.InternalConversionException;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class GCatModel implements CatalogueFormatData {

	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	public static class CkanResource {

		private String name;
		private String url;
		private String format;
		private String description;
		
	}
	
	
	private static ObjectMapper mapper=new ObjectMapper();
	
	@Setter
	@Getter
	private String profile=null;
	
	@Setter
	@Getter
	private CkanItem item;
	
	@Setter
	@Getter
	private ArrayList<CkanResource> resources=new ArrayList<>();
	
	@Override
	public String toCatalogueFormat() throws InternalConversionException {
		try{
			ByteArrayOutputStream baos=new ByteArrayOutputStream();
			mapper.writeValue(baos, this);
			return baos.toString();
		}catch(Throwable t) {
			throw new InternalConversionException("Unable to convert",t);
		}
	}
}
