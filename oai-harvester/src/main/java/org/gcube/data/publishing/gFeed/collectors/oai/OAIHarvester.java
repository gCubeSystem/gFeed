package org.gcube.data.publishing.gFeed.collectors.oai;

import java.io.InputStream;
import java.util.Collections;
import java.util.Set;

import org.gcube.data.publishing.gCatFeeder.model.CatalogueFormatData;
import org.gcube.data.publishing.gCatFeeder.model.ControllerConfiguration;
import org.gcube.data.publishing.gCatFeeder.model.EnvironmentConfiguration;
import org.gcube.data.publishing.gCatFeeder.utils.IOUtils;
import org.gcube.data.publishing.gCatfeeder.collectors.CatalogueRetriever;
import org.gcube.data.publishing.gCatfeeder.collectors.CollectorPlugin;
import org.gcube.data.publishing.gCatfeeder.collectors.DataCollector;
import org.gcube.data.publishing.gCatfeeder.collectors.DataTransformer;
import org.gcube.data.publishing.gCatfeeder.collectors.model.PluginDescriptor;
import org.gcube.data.publishing.gCatfeeder.collectors.model.faults.CatalogueNotSupportedException;
import org.gcube.data.publishing.gFeed.collectors.oai.model.OAIRecord;
import org.gcube.data.publishing.gFeed.collectors.oai.model.ckan.GCatTransformer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OAIHarvester implements CollectorPlugin<OAIRecord>{

	@Override
	public PluginDescriptor getDescriptor() {
		return new PluginDescriptor(Constants.PLUGIN_ID);
	}

	@Override
	public CatalogueRetriever getRetrieverByCatalogueType(String catalogueType) throws CatalogueNotSupportedException {
		switch(catalogueType) {
		case Constants.GCAT_TYPE : return GCATRetriever.get();
		default : throw new CatalogueNotSupportedException("No support for "+catalogueType); 
		}
	}

	@Override
	public Set<String> getSupportedCatalogueTypes() {
		return Collections.singleton(Constants.GCAT_TYPE);
	}

	@Override
	public DataTransformer<? extends CatalogueFormatData, OAIRecord> getTransformerByCatalogueType(String catalogueType)
			throws CatalogueNotSupportedException {
		switch(catalogueType) {
		case Constants.GCAT_TYPE : return new GCatTransformer();
		default : throw new CatalogueNotSupportedException("No support for "+catalogueType); 
		}
	}

	@Override
	public DataCollector<OAIRecord> getCollector() {
		return new OAICollector();
	}

	@Override
	public ControllerConfiguration getPublisherControllerConfiguration(String catalogueType)
			throws CatalogueNotSupportedException {
		return new ControllerConfiguration();
	}

	@Override
	public void init() throws Exception {
		
		String harvestedObjectProfile=IOUtils.readStream(
		getClass().getClassLoader().getResourceAsStream("profiles/HarvestedObject.xml"));
		
		
		
		if(harvestedObjectProfile==null||harvestedObjectProfile.isEmpty())
			log.warn("HarvestedObject is empty");
		
		Constants.xmlProfiles.put("HarvestedObject", harvestedObjectProfile);	
		Set<String> profilesID=Constants.xmlProfiles.keySet();
		log.info("Loaded profiles "+profilesID);
		if(log.isDebugEnabled())
			for(String p:profilesID)
				log.debug("PROFILE "+p+" : "+Constants.xmlProfiles.get(p));
		
		
	}

	
	@Override
	public void initInScope() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEnvironmentConfiguration(EnvironmentConfiguration env) {
		// TODO Auto-generated method stub
		
	}

}
