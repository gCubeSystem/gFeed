package org.gcube.data.publishing.gFeed.collectors.oai;

import java.util.HashMap;
import java.util.Map;

public class Constants {


	public static final String GCAT_TYPE="GCAT";
	
	public static final String PLUGIN_ID="OAI_COLLECTOR"; 
	
	
	public static final Map<String,String> xmlProfiles=new HashMap<String,String>();
}
