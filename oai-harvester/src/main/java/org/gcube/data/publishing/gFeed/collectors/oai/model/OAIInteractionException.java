package org.gcube.data.publishing.gFeed.collectors.oai.model;

public class OAIInteractionException extends Exception {

	public OAIInteractionException() {
		// TODO Auto-generated constructor stub
	}

	public OAIInteractionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public OAIInteractionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public OAIInteractionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public OAIInteractionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
