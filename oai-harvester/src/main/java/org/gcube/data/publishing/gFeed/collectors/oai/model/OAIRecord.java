package org.gcube.data.publishing.gFeed.collectors.oai.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.gcube.data.publishing.gCatfeeder.collectors.model.CustomData;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@XmlRootElement(name = "record", namespace=Namespaces.OAI_PMH_NS)
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class OAIRecord implements CustomData{
	
	
	@NoArgsConstructor
	@AllArgsConstructor
	@ToString
	@Getter
	@Setter	
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlRootElement(namespace=Namespaces.OAI_PMH_NS)	
	public static class Header{
		@XmlElement(namespace=Namespaces.OAI_PMH_NS)
		private String identifier;
		@XmlElement(name = "datestamp", namespace=Namespaces.OAI_PMH_NS)
		private String dateStamp;
		@XmlElement(namespace=Namespaces.OAI_PMH_NS)
		private List<String> setSpec;
	}
	
	
	@XmlElement(namespace=Namespaces.OAI_PMH_NS)
	private Header header;
	@XmlElement(name = "metadata", namespace=Namespaces.OAI_PMH_NS)
	public MetadataHolder metadata;
}
