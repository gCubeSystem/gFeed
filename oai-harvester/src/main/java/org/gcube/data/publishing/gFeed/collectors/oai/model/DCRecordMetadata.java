package org.gcube.data.publishing.gFeed.collectors.oai.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter	
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "dc", namespace="http://www.openarchives.org/OAI/2.0/oai_dc/")
public class DCRecordMetadata extends OAIMetadata{

	public static final String NS="http://purl.org/dc/elements/1.1/";
	
	@XmlElement(namespace=NS)
	private List<String> contributor;
	@XmlElement(namespace=NS)
	private List<String> coverage;
	@XmlElement(namespace=NS)
	private List<String> creator;
	@XmlElement(namespace=NS)
	private List<String> date;
	@XmlElement(namespace=NS)
	private List<String> description;
	@XmlElement(namespace=NS)
	private List<String> format;
	@XmlElement(namespace=NS)
	private List<String> identifier;
	@XmlElement(namespace=NS)
	private List<String> language;
	@XmlElement(namespace=NS)
	private List<String> publisher;

	@XmlElement(namespace=NS)
	private List<String> relation;
	@XmlElement(namespace=NS)
	private List<String> rights;
	@XmlElement(namespace=NS)
	private List<String> source;
	@XmlElement(namespace=NS)
	private List<String> subject;
	
	@XmlElement(namespace=NS)
	private List<String> title;
	@XmlElement(namespace=NS)
	private List<String> type;
	
	
}
