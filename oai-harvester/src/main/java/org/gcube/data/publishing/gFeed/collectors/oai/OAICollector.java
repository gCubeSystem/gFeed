package org.gcube.data.publishing.gFeed.collectors.oai;

import java.util.HashSet;
import java.util.Set;

import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.Property;
import org.gcube.data.publishing.gCatFeeder.utils.ISUtils;
import org.gcube.data.publishing.gCatfeeder.collectors.DataCollector;
import org.gcube.data.publishing.gCatfeeder.collectors.model.faults.CollectorFault;
import org.gcube.data.publishing.gFeed.collectors.oai.model.OAIRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OAICollector implements DataCollector<OAIRecord> {

	@Override
	public Set<OAIRecord> collect() throws CollectorFault {
		try {
			HashSet<OAIRecord> toReturn=new HashSet<OAIRecord>();
			String oaiCategory="Repository";
			String oaiPlatform="oai-pmh";
			for(ServiceEndpoint epr:ISUtils.queryForServiceEndpoints(oaiCategory, oaiPlatform)) {
				log.info("Found OAI Repo in resource "+epr.id()+" NAME : "+epr.profile().name());
				AccessPoint point=epr.profile().accessPoints().asCollection().iterator().next();
				String baseUrl=point.address();
				log.debug("Address is "+baseUrl);
				try {
					OAIClient client = new OAIClient(baseUrl);
					point.properties().iterator().forEachRemaining((Property p)->{
						if(p.name().equals("set"))
							client.getSpecifiedSets().add(p.value());
					});
					toReturn.addAll(client.getAll(OAIClient.DC_METADATA_PREFIX));
				}catch(Throwable t) {
					log.warn("Unable to use repository "+epr.id()+" NAME : "+epr.profile().name(),t);
				}
			}

			return toReturn;
		}catch(Throwable t) {
			throw new CollectorFault(t);
		}
	}




}
