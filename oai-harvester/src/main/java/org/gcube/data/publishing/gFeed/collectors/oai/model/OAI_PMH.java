package org.gcube.data.publishing.gFeed.collectors.oai.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@XmlRootElement(name="OAI-PMH", namespace=Namespaces.OAI_PMH_NS)
//@XmlRootElement(name="OAI-PMH")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class OAI_PMH {

	@NoArgsConstructor
	@AllArgsConstructor
	@ToString
	@Getter
	@Setter	
	@XmlAccessorType(XmlAccessType.FIELD)	
	public static class Request{
		@XmlAttribute
		private String metadataPrefix;
		@XmlAttribute
		private String verb;
		@XmlValue
		private String path;
		
	}
	
	@NoArgsConstructor
	@AllArgsConstructor
	@ToString
	@Getter
	@Setter	
	@XmlAccessorType(XmlAccessType.FIELD)	
	public static class Token{
		@XmlAttribute
		private int cursor;
		@XmlValue
		private String id;
	}
	
	
	@NoArgsConstructor
	@AllArgsConstructor
	@ToString
	@Getter
	@Setter	
	@XmlAccessorType(XmlAccessType.FIELD)	
	public static class ListRecords{
		@XmlElement(name = "record",namespace=Namespaces.OAI_PMH_NS)
		private List<OAIRecord> records;
		@XmlElement(name = "resumptionToken",namespace=Namespaces.OAI_PMH_NS)
		private Token resumptionToken;
	}
	
	
	@NoArgsConstructor
	@AllArgsConstructor
	@ToString
	@Getter
	@Setter	
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Error{
		@XmlAttribute
		private String code;
		@XmlValue
		private String message;
	}
	
	
	@XmlElement(namespace=Namespaces.OAI_PMH_NS)
	private String responseDate;
	
	@XmlElement(namespace=Namespaces.OAI_PMH_NS)
	private Request request;
	
	@XmlElement(namespace=Namespaces.OAI_PMH_NS)
	private Error error;
	
	@XmlElement(name="ListRecords", namespace=Namespaces.OAI_PMH_NS)
	private ListRecords responseRecords;
	

	public boolean isError() {
		return error!=null;
	}
}
