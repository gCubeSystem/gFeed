package org.gcube.data.publishing.gFeed.collectors.oai.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter	
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "metadata", namespace="http://www.openarchives.org/OAI/2.0/")	
public class MetadataHolder{
	@XmlElementRef
//	@XmlElements({
//	@XmlElement(name="dc",namespace="http://www.openarchives.org/OAI/2.0/oai_dc/", type=DCRecordMetadata.class)
//	})
	public OAIMetadata metadata;
}