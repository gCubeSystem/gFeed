package org.gcube.data.publishing.gFeed.collectors.oai.model.ckan;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.gcube.data.publishing.gCatFeeder.utils.Files;
import org.gcube.data.publishing.gCatfeeder.collectors.DataTransformer;
import org.gcube.data.publishing.gFeed.collectors.oai.Constants;
import org.gcube.data.publishing.gFeed.collectors.oai.model.DCRecordMetadata;
import org.gcube.data.publishing.gFeed.collectors.oai.model.OAIMetadata;
import org.gcube.data.publishing.gFeed.collectors.oai.model.OAIRecord;
import org.gcube.data.publishing.gFeed.collectors.oai.model.ckan.CkanItem.CKanExtraField;
import org.gcube.data.publishing.gFeed.collectors.oai.model.ckan.CkanItem.Tag;
import org.gcube.data.publishing.gFeed.collectors.oai.model.ckan.GCatModel.CkanResource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GCatTransformer implements DataTransformer<GCatModel,OAIRecord>{



	@Override
	public Set<GCatModel> transform(Collection<OAIRecord> collectedData) {
		boolean useProfile=true;

		HashSet<GCatModel> toReturn=new HashSet<>();
		for(OAIRecord record:collectedData) {
			GCatModel toPublish=translate(record,useProfile);
			if(useProfile) {
				useProfile=false;
			}
			toReturn.add(toPublish);
		}
		return toReturn;
	}


	/**
	 * (Common) Title
	 * (Common) Description
	 * (Common) Tags: free list of keywords
	 * (Common) License 
	 * (Common) Visibility: either public or private
	 * (Common) Version
	 * (Common) Author: the creator of metadata. Only one occurrence is supported; 
	 * (Common) Maintainer: 
	 * (Method specific) Creator: the author of the method (with email and ORCID). Repeatable field; 
	 * (Method specific) Creation date: when the method has been released; 
	 * (Method specific) Input: Repeatable field;
	 * (Method specific) Output: Repeatable field; 
	 * (Method specific) RelatedPaper: a reference to an associated paper;
	 * (Method specific) Restrictions On Use: an optional text 
	 * (Method specific) Attribution requirements: the text to use to acknowledge method usage; 
	 */
	private static GCatModel translate(OAIRecord toTranslate,Boolean useProfile) {
		GCatModel toReturn = new GCatModel();

		CkanItem item=new CkanItem();
		//escaping name chars
		String toSetName=toTranslate.getHeader().getIdentifier();
		toSetName=toSetName.toLowerCase().replaceAll("[^a-z0-9_\\\\-]", "_");
		item.setName(toSetName);
		OAIMetadata meta=toTranslate.getMetadata().getMetadata();
		if(meta instanceof DCRecordMetadata) {
			String profileID="Harvested Object";

			DCRecordMetadata dcMeta=(DCRecordMetadata) meta;

			item.setTitle(getFirstOrDefault(dcMeta.getTitle(),"n.a."));
			item.setNotes(getFirstOrDefault(dcMeta.getDescription(),null));
			item.setAuthor(getFirstOrDefault(dcMeta.getPublisher(),"n.a."));
			item.setMaintainer(getFirstOrDefault(dcMeta.getPublisher(),null));
			item.setVersion("n.a.");
			item.setPrivateFlag(false);
			item.setLicense_id("CC-BY-NC-SA-4.0");

			if(dcMeta.getSubject()!=null&&dcMeta.getSubject().isEmpty())
				for(String subject:dcMeta.getSubject())
					item.getTags().add(new Tag(subject));
			
			item.getTags().add(new Tag("OAI"));
			
			item.getExtras().add(new CKanExtraField("system:type", profileID));


			item.getExtras().addAll(fromList(dcMeta.getContributor(),profileID+":contributor"));
			item.getExtras().addAll(fromList(dcMeta.getCoverage(),profileID+":coverage"));
			item.getExtras().addAll(fromList(dcMeta.getCreator(),profileID+":creator"));
			item.getExtras().addAll(fromList(dcMeta.getDate(),profileID+":date"));
			item.getExtras().addAll(fromList(dcMeta.getDescription(),profileID+":description"));
			item.getExtras().addAll(fromList(dcMeta.getFormat(),profileID+":format"));
			item.getExtras().addAll(fromList(dcMeta.getIdentifier(),profileID+":identifier"));
			item.getExtras().addAll(fromList(dcMeta.getLanguage(),profileID+":language"));
			item.getExtras().addAll(fromList(dcMeta.getPublisher(),profileID+":publisher"));
			item.getExtras().addAll(fromList(dcMeta.getRelation(),profileID+":relation"));
			item.getExtras().addAll(fromList(dcMeta.getRights(),profileID+":rights"));
			item.getExtras().addAll(fromList(dcMeta.getSource(),profileID+":source"));
			item.getExtras().addAll(fromList(dcMeta.getSubject(),profileID+":subject"));
			item.getExtras().addAll(fromList(dcMeta.getTitle(),profileID+":title"));
			item.getExtras().addAll(fromList(dcMeta.getType(),profileID+":type"));



			if(dcMeta.getIdentifier()!=null) {
				ArrayList<CkanResource> list=new ArrayList<>();
				for(String id:dcMeta.getIdentifier()) {
					try {
						URL url=new URL(id);
						CkanResource res=new CkanResource("Record", url+"", url.getProtocol(), "Original record");
						list.add(res);
					}catch(Throwable t) {
						log.debug("Unable to set identifier "+id+"as resource ",t);
					}
				}
				toReturn.setResources(list);
			}

		}

		toReturn.setItem(item);

		try {
			if(useProfile)
				toReturn.setProfile(Constants.xmlProfiles.get("HarvestedObject")); //"Harvested Object”
		}catch(Throwable t) {
			log.error("Unable to set profile ",t);

		}
		return toReturn;
	}



	private static Collection<CKanExtraField> fromList(List<String> values,String name){
		ArrayList<CKanExtraField> toReturn=new ArrayList<CKanExtraField>();
		if(values!=null)
			for(String v:values)
				toReturn.add(new CKanExtraField(name,v));
		return toReturn;
	}
	
	private static String getFirstOrDefault(List<String> values,String defaultValue) {
		if(values!=null && !values.isEmpty()) return values.get(0);
		else return defaultValue;
	}
}
