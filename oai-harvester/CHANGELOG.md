This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.data-publishing.gFeed.oai-pmh

## [v1.0.8]
Harvested Object profile update


## [v1.0.7]
- Pom updates

## [v1.0.6]
- Pom updates

## [v1.0.5] - 2020-12-15
Do not stop on single repository error

## [v1.0.4] - 2020-12-15
- Dependency management
- Naming Convention
- Support to Set Filtering [#20342]