package org.gcube.data.publishing.gCatFeeder.model;

public class InternalConversionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5731594279138149384L;

	public InternalConversionException() {
		// TODO Auto-generated constructor stub
	}

	public InternalConversionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InternalConversionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InternalConversionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InternalConversionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
