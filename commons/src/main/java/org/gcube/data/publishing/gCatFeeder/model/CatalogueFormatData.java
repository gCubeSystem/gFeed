package org.gcube.data.publishing.gCatFeeder.model;

public interface CatalogueFormatData {

	
	public String toCatalogueFormat() throws InternalConversionException;
	
}
