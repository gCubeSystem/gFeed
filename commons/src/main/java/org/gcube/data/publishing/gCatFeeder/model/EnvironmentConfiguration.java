package org.gcube.data.publishing.gCatFeeder.model;

import java.util.Map;

public interface EnvironmentConfiguration {

	public Map<String,String> getCurrentConfiguration();
	
}
