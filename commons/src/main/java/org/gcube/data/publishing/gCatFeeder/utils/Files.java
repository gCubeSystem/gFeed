package org.gcube.data.publishing.gCatFeeder.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Files {

	private static final Logger log= LoggerFactory.getLogger(Files.class);
	
	//Classloader & resources cause problems with plugins
//	public static String getProjectDir() {
//	        try {
//	            Class<?> callingClass = Class.forName(Thread.currentThread().getStackTrace()[2].getClassName());
//	            URL url = callingClass.getProtectionDomain().getCodeSource().getLocation();
//	            URI parentDir = url.toURI().resolve("..");          
//	            return parentDir.getPath();
//	        } catch (ClassNotFoundException e) {
//	            e.printStackTrace();
//	        } catch (URISyntaxException e) {
//	            e.printStackTrace();
//	        }
//	        return "";
//	}
	
	
//	public static String readFileFromResources(String fileName) throws IOException {
//
//        ClassLoader classLoader =Files.class.getClassLoader();
//
//        URL resource = classLoader.getResource(fileName);
//        if (resource == null) {
//            throw new IllegalArgumentException("file is not found!");
//        } 
//        return readStream(resource.openStream());
//
//    }	

//	public static final String readStream(InputStream is) throws IOException {
//		byte[] buffer = new byte[10];
//		StringBuilder sb = new StringBuilder();
//		while (is.read(buffer) != -1) {
//			sb.append(new String(buffer));
//			buffer = new byte[10];
//		}
//		is.close();
//
//		return sb.toString();
//	}
//	
//	public static String readFileAsString(String toRead) 
//			  throws IOException{
//	
//		String fullPath=getProjectDir()+toRead;
//		log.debug("Reading "+toRead+". Full path is "+fullPath);
//		
//		
//		InputStream fis = new FileInputStream(toRead);
//		return readStream(fis);
//		
//	}
	

//	public static String readFileAsString(String path, Charset encoding) 
//			  throws IOException 
//	{
//	  byte[] encoded = java.nio.file.Files.readAllBytes(Paths.get(path));
//	  return new String(encoded, encoding);
//	}
//	
	
	public static String getName(String path) {
		
		return path.substring((path.contains(File.separator)?path.lastIndexOf(File.separator)+1:0)
				,(path.contains(".")?path.lastIndexOf("."):path.length()));
		
	}
}
