This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gFeed

## [v1.0.6]
- Pom updates

## [v1.0.5]
- Pom updates

## [v1.0.4] - 2020-12-15
- Dependency management
- Fixed naming

## [v1.0.3] - 2020-05-15

### New Features
- OAI-PMH harvester plugin for gFeed (https://support.d4science.org/issues/19351)

### Fixes

- Pom versions in module is now parent
- Dependencies between suite use same version
- Resources are now read as InputSream in Common module

